public class Enemy extends Actor {
	protected String imageFile = "src/meteorite.png";
	private int step = 5;
	public Enemy() {
		super.setImage(imageFile);
		super.setStep(step);
	}

	public Enemy(String avatar) {
		this.imageFile = avatar;
		super.setImage(imageFile);
		super.setStep(step);
	}
	public Enemy (int x, int y){
		this();
		super.setX(x);
		super.setY(y);
	}
}
