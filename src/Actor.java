import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


import javax.imageio.ImageIO;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public abstract class Actor {
	private String shootingSoundFile = "src/scifi002.wav";
	protected BufferedImage image;
	private int x=100, y=100, step=10;
	public Actor() {
	}

	public void shoot() {
		try {
			AudioPlayer.player.start(new AudioStream(new FileInputStream(shootingSoundFile)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public BufferedImage getImage(){
		return image;
	}
	public void setImage(String imageFile) {
		try {
			image = ImageIO.read(new File(imageFile));
		System.out.println("here");
		} catch (Exception e) {
			System.out.println("Could not load image from " + imageFile);
		}
		
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}
	

}
