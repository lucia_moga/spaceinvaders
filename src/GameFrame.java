import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class GameFrame extends JFrame {

	private JPanel contentPane;

	public GameFrame(int width, int height) {
		setResizable(false);
		//setAutoRequestFocus(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, width, height);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel menuPanel = new JPanel();
		contentPane.add(menuPanel, BorderLayout.NORTH);
		
		JLabel lblLuciaIsThe = new JLabel("Score: ");
		lblLuciaIsThe.setHorizontalAlignment(SwingConstants.LEFT);
		menuPanel.add(lblLuciaIsThe);
		
		JLabel scoreLabel = new JLabel("1:3 (dummy text - replace me)");
		menuPanel.add(scoreLabel);
		//JPanel gamePanel = new JPanel();
		GamePanel gamePanel = new GamePanel();
		gamePanel.setForeground(Color.BLACK);
		contentPane.add(gamePanel, BorderLayout.CENTER);
		this.setVisible(true);
		
	}
}
