import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JPanel;
import javax.swing.Timer;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class GamePanel extends JPanel implements KeyListener {
	private static final int NO_ENEMIES = 10;

	private static final String backgroundImage = "src/magellanic-clouds.png";
	private static final Color backgroundColor = Color.black;
	private static final boolean solidColorBackground =false;
	public static final int moveSize = 10;
	private Player localPlayer;

	private int direction = 1;

	
	private ArrayBlockingQueue<Enemy> enemies = new  ArrayBlockingQueue<Enemy>(100);
	private ArrayList<Player> players = new ArrayList<Player>();
	
	public GamePanel (ArrayList<Player> players, ArrayBlockingQueue<Enemy> enemies){
		this();
		this.players = players;
		this.enemies = enemies;
	}
	public GamePanel() {
		addKeyListener(this);
		setVisible(true);
		setFocusable(true);
		requestFocus(true);
		requestFocusInWindow();

		localPlayer = new Player(150, 300);
		players.add(localPlayer);
		generateEnemies(15);

		// MOVE FROM LEFT TO RIGHT REPEATEDLY - ONE MOVE FOR EACH TICK OF THE
		// TIMER
		 Timer timer = new Timer(50, new ActionListener() {
		 @Override
		 public void actionPerformed(ActionEvent e) {
			 for (Enemy enemy : enemies) {
				 enemy.setY(enemy.getY() + enemy.getStep());
				 if (enemy.getY() > getHeight() ){
					 enemy.setY(0);
					//enemies.remove(enemy);
					//generateEnemies(1);
				 }
			 }
		 
		 repaint();
		 
		 }
		
		 });
		 timer.setRepeats(true);
		 timer.setCoalesce(true);
		 timer.start();
		// COMMENT PREVIOUS BLOCK TO MOVE ONLY BY USING THE KEYS

	}
	// //@Override
	// public Dimension getPreferredSize() {
	//// return player == null ? super.getPreferredSize()
	//// : new Dimension(WIDTH, HEIGHT);
	// return super.getPreferredSize();
	// }
	public void generateEnemies(int n){
		Random rand = new Random();
		for (int i=0; i<=n; i++){
			int x = rand.nextInt(700);
			int y = rand.nextInt(400);
			Enemy enemy = new Enemy(x, y);
			enemies.add(enemy);
		}
	}
	public void addEnemy(Enemy enemy){
		enemies.add(enemy);
	}
	public void addPlayer(Player player){
		players.add(player);
	}
	
	public void paintEnemies(Graphics g){
		for (Enemy enemy : enemies) {
			System.out.println(enemy.getX() + " " + enemy.getY());
			 g.drawImage(enemy.getImage(), enemy.getX(), enemy.getY(), this);
		}
	}
	public void paintPlayers(Graphics g){
		for (Player player : players) {
			 g.drawImage(player.getImage(), player.getX(), player.getY(), this);
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		// super.paintComponent(g);

		// setOpaque(false);
		// setBackground(Color.black);
		
		if (solidColorBackground){
			setBackground(backgroundColor);
		}
		else{
		try {
			BufferedImage background = ImageIO.read(new File(backgroundImage));
			g.drawImage(background, 0, 0, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		}
		//
		paintPlayers(g);
		paintEnemies(g);
		//g.drawImage(player.getImage(), playerPos, getHeight() - player.getImage().getHeight() - 50, this);

		g.setColor(Color.WHITE);
		 Font font = new Font("Courier", Font.BOLD, 15);
	      g.setFont(font);
		g.drawString("Alex", localPlayer.getX()+10, getHeight() - 30);
	}

	public void playSound(AudioStream audioStream) {
		AudioPlayer.player.start(audioStream);
	}

	@Override
	public void keyPressed(KeyEvent key) {
		int keyCode = key.getKeyCode();
		
		System.out.println(keyCode);
		switch (keyCode) {
//		case 38:
//			//UP
//			break;
//		case 40:
//			//DOWN
//			break;
		case 37:
			localPlayer.setX(localPlayer.getX() - localPlayer.getStep());
			break;
		case 39:
			localPlayer.setX(localPlayer.getX() + localPlayer.getStep());
			break;
		case 32:
			localPlayer.shoot();
			break;
		}
		repaint();

	}

	@Override
	public void keyReleased(KeyEvent key) {

	}

	@Override
	public void keyTyped(KeyEvent key) {

	}
}