

public class Player extends Actor {
	protected String imageFile = "src/rocket.png";
	private int step = 10;

	public Player() {

		super.setImage(imageFile);
		super.setStep(step);
	}
	public Player (int x, int y){
		this();
		super.setX(x);
		super.setY(y);
	}
	

}
